AStA Server Setup
=================

Ansible Rolle fürs generische Server Setup des AStAs

Siehe auch das [AStA Ansible](https://gitlab.gwdg.de/asta/dnd-referat/asta-ansible) Projekt.

Requirements
------------

Role Variables
--------------

### Generelle Variablen

- `autorestart_not_before`(String, default: '2332800'): Minimale Serveruptime (in Sekunden) bevor ein Server am 01. des Monats neustartet. (2332800 Sekunden = 27 Tage)
- `apt_cache_valid_time`(String, default: '86400'): . Siehe https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html#parameter-cache_valid_time . (86400 Sekunden = 1 Tag)
- `default_editor`(String, default: '/bin/nano'): Standard Texteditor welcher auf den Servern verwendet werden soll. Führe `update-alternatives --list editor` auf einem Server aus, um zu sehen welche Werte möglich sind.

### Hostspezifische Variablen

Jeder Host im Inventory **MUSS** die folgenden Variablen gesetzt haben:

- `public_ip`(String): Die öffentliche IP Adresse des Servers.
- `public_domains`(List\<String\>): Die Domains welche auf den Server zeigen.
- `asta_meta`(Object): 
	- `title`(String): Servername/Titel
	- `responsible`(List\<String\>): Namen der Personen die für den Server verantwortlich sind.
	- `description`(List\<String\>): Serverbeschreibung in Stichpunkten
	- `account`(String): Der Funktionsaccount/Nutzer über den der Server erstellt wurde.

**Wichtig:** Wenn bei einem Eintrag eine Liste erwartet wird, dann **MUSS** das auch eine Liste sein!
Selbst wenn z.B. nur eine Person für einen Server verwantwortlich ist!

Dependencies
------------

Diese Rolle verwendet [msmtp](https://galaxy.ansible.com/chriswayg/msmtp-mailer) und [unattended-upgrades](https://galaxy.ansible.com/jnv/unattended-upgrades).
Entsprechend werden auch die Variablen von dieses Rollen verwendet.

Welche Variablen das sind können in den entsprechenden READMEs nachgelesen werden:

- [README von msmtp](https://github.com/chriswayg/ansible-msmtp-mailer#readme)
- [README von unattended-upgrades](https://github.com/jnv/ansible-role-unattended-upgrades#readme)


Example Playbook
----------------

Das [AStA Ansible](https://gitlab.gwdg.de/asta/dnd-referat/asta-ansible) Projekt nutzt diese Rolle.

License
-------

CC0-1.0

Author Information
------------------

Digitalisierungsreferat des [AStA Uni Göttingen](https://asta.uni-goettingen.de/).

